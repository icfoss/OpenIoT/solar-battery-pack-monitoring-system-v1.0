## Solar-Battery Pack Monitoring System_v1.0
<br>

### Prerequisites
 -----------------
  - [Arduino IDE v1.8.19](https://www.arduino.cc/en/software)
  - [MCCI lorawan lmic library v4.0.0](https://github.com/mcci-catena/arduino-lmic)
  - [ULP LoRa](https://gitlab.com/icfoss/OpenIoT/ulplora)
  - Battery Management system
  - Battery Pack
  - Solar Panel

### Getting Started 
----
- Connect the components as shown in the ***Wiring diagram*** [here]()
- Install ***Arduino IDE***.[here](https://www.arduino.cc/en/Guide/)
- Install ***MCCI lorawan lmic library*** [here](https://docs.arduino.cc/software/ide-v1/tutorials/installing-libraries)
- config the library using ***lmic_project_config.h*** inside the ***project_config*** folder.[here](https://github.com/mcci-catena/arduino-lmic#configuration)
- Select Board : ***Arduino Pro or Pro Mini***
* **Configuration** 

    ```sh
    # Set Transmission interval
    const unsigned long TX_INTERVAL = 180; 
    
    # Set Network session key in Hex Array Format
    static const PROGMEM u1_t NWKSKEY[16] = { 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00 };

    # Set App session key in Hex Array Format
    static const u1_t PROGMEM APPSKEY[16] = { 0x00, 0x0,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00 };

    # Set Device address
    static const u4_t DEVADDR = 0x00000000 ;    
    ```
    <br>

    ```sh
    # If using external radio module do change the pin
    # configuration accordingly

    const lmic_pinmap lmic_pins = {
    .nss = 6,                       # chip select on 
    feather (rf95module) CS
    .rxtx = LMIC_UNUSED_PIN,
    .rst = 5,                       # reset pin
    .dio = {2, 3, LMIC_UNUSED_PIN},
    };
    ```
- Upload the program




