/**
  ******************************************************************************
  * @file	Things_UNO_Solar_Charge_Controller_Monitor_V1.0.0.ino
  * @author CDOH Team
  * @brief  Code to collect data from a battery management system
            using ThingsUno board and transmit the data via LoRa Communication
  *@verbatim
  ==============================================================================
                        ##### How to use #####
  ==============================================================================
 	 	 	(+)  Set the transmission interval in milli seconds
           const long transmission_interval  = 600000; 
 	 	 	(+) Set the device address in string format
          const char *devAddr = "00000000";
 	 	 	(+) Set the network session key in string format
          const char *nwkSKey = "00000000000000000000000000000000";
 	 	 	(+) Set the app session key in string format
          const char *appSKey = "00000000000000000000000000000000";
 **/


#include <TheThingsNetwork.h>
#include <SoftwareSerial.h>

// SoftwareSerial Serial_name(RX_Pin, TX_Pin);

SoftwareSerial mySerial1(10, 11);

// variable to store incoming data

int incomingByte = 0; 

byte payload[4];

unsigned long previousTime = 0;
unsigned long previous_serial_time = 0;
const long transmission_interval  = 600000;

// Set your DevAddr, NwkSKey, AppSKey and the frequency plan

const char *devAddr = "00000000";
const char *nwkSKey = "00000000000000000000000000000000";
const char *appSKey = "00000000000000000000000000000000";

#define loraSerial Serial1
#define debugSerial

// Replace REPLACE_ME with TTN_FP_EU868 or TTN_FP_US915 or TTN_FP_IN865_867

#define freqPlan REPLACE_ME

//set spreading factor 0 to 5 represents sf12 to sf7

#define sf 0

TheThingsNetwork ttn(loraSerial, Serial, freqPlan);

void setup()
{
  loraSerial.begin(9600);
  mySerial1.begin(9600);
#ifdef debugSerial
  Serial.begin(9600);
  while (!Serial && millis() < 10000);   // Wait a maximum of 10s for Serial Monitor
  Serial.println("starting");
  Serial.println("-- PERSONALIZE");
#endif

  ttn.personalize(devAddr, nwkSKey, appSKey);
  ttn.setDR (sf);

#ifdef debugSerial
  Serial.println("-- STATUS");
  ttn.showStatus();
#endif

  previous_serial_time = millis();

// Function to collect data from the BMS (battery management system)

  data(); 

// Uplink data function

  transmission(); 
}

void loop()
{

#ifdef debugSerial
  Serial.println("-- LOOP");
#endif

  unsigned long currentTime = millis();
  if (currentTime < previousTime)
  {
    previousTime = 0;
  }

  else if ((currentTime - previousTime) >= transmission_interval)
  {
    transmission();
  }
  else
  {
    data();
  }

}

/**
  * @brief  Read Data From the BMS
  * @param  none
  * @retval error code
  */
int data()
{
  
  /**
   * Solar charge controller Serial Transmission Data Format
   * 
   * Starting Value = 21
   * Solar Voltage
   * Battery Voltage
   * Battery Current
   * Solar Current
   * End of Measurement Value = 75
   * **/

  int i = 0, flag = 0;
  unsigned long current_serial_time;
  while (1)
  {
    current_serial_time = millis();
    if (mySerial1.available() > 0)
    {
      incomingByte = (int)mySerial1.read();
      delay(500);

      if (flag == 1)
      {
        payload[i] = incomingByte;
        i++;
      }

      if (incomingByte == 21)
      {
        flag = 1;
        i = 0;
      }
      else if (incomingByte == 75)
      {
        flag = 0;
        previous_serial_time = current_serial_time;
        return 0;
      }
    }
    else
    {
      // Exit from the loop and  transmit Error Value 255 if Serial communication isn't available for more than 30 seconds 

      if ((current_serial_time - previous_serial_time) >= 30000)
      {
        for (i = 0; i < 4; i++)
          payload[i] = 255;
        previous_serial_time = current_serial_time;
        return 0;
      }
    }
  }
}

/**
  * @brief  Transmit Data
  * @param  none
  * @retval error code
  */

int transmission()
{
#ifdef debugSerial
  Serial.print("SV = ");
  Serial.println(payload[0]);
  Serial.print("BV ");
  Serial.println(payload[1]);
  Serial.print("SI = ");
  Serial.println(payload[2]);
  Serial.print("BI = ");
  Serial.println(payload[3]);
#endif

// ttn.sendBytes(payload, sizeof(payload), port);

  ttn.sendBytes(payload, sizeof(payload), 2);
  for (int i = 0; i < 4; i++)
    payload[i] = 0;
  previousTime = millis();
  previous_serial_time = millis();
  delay(1000);
  return 0;
}
