## Solar-Battery Pack Monitoring System_v1.0


### Prerequisites
----
  - [Arduino IDE v1.8.19](https://www.arduino.cc/en/software)
  - [The Things Network library v2.4.1](https://github.com/TheThingsNetwork/arduino-device-lib)
  - [The Things UNO board](https://connectedthings.store/gb/lorawan-development/the-things-uno-development-board.html)
  - Battery Management system
  - Battery Pack
  - Solar Panel

### Getting Started
----
- Connect the components as shown in the wiring diagram [here]()
- Install Arduino IDE.[here](https://www.arduino.cc/en/Guide/)
- Install The Things Network library [here](https://docs.arduino.cc/software/ide-v1/tutorials/installing-libraries)
- Select Board : Arduino Leornado ETH
* **Configuration**

```sh
# Set the transmission interval
const long transmission_interval  = 600000; 

# Set the device address in string format
const char *devAddr = "00000000";

# Set the network session key in string format
const char *nwkSKey = "00000000000000000000000000000000";

# Set the app session key in string format
const char *appSKey = "00000000000000000000000000000000";
```
- Upload the program



