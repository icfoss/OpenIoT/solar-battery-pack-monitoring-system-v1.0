## Solar-Battery Pack Monitoring System_v1.0
<br>

![Solar Battery Pack](https://gitlab.com/abdularshadvs/solar-battery-pack-monitoring-system-v1.0/-/raw/main/Images/battery_pack.png?ref_type=heads)
### Overview
------------

This system enables remote monitoring of solar panel performance through the utilization of an Arduino-compatible microcontroller integrated with the LoRaWAN radio module. This combination enables efficient wireless communication over the LoRaWAN network, streamlining the process of collecting essential data from the battery management system.


### Features
------------

- LoRa compatible
- Easy real time performance monitoring 

### Recorded Parameters
-----------------------

- Solar voltage
- Solar current
- Battery voltage
- Battery current

### Supported Radio Module
--------------------------

* RFM95w
* RFM92
* SX1276
* SX1272
* RN2483
* RN2903

### Supported Region
--------------------
- eu868
- us915
- as_923
- as_923jp
- kr_920
- in_866


### Prerequisites
-----------------

  - [Arduino IDE 1.8.19](https://www.arduino.cc/en/software)
  - Battery Management system
  - Battery Pack
  - Solar Panel
  - For Things UNO Board [here](https://gitlab.com/abdularshadvs/solar-battery-pack-monitoring-system-v1.0/-/tree/main/Software/Things_Uno_Board?ref_type=heads#prerequisites)
  - For Arduino Microcontrollers [here](https://gitlab.com/abdularshadvs/solar-battery-pack-monitoring-system-v1.0/-/tree/main/Software/ULP_LoRa_Board?ref_type=heads#prerequisites)


### Getting Started
-------------------

- Install Arduino IDE.
- For Things UNO Board [here](https://gitlab.com/abdularshadvs/solar-battery-pack-monitoring-system-v1.0/-/tree/main/Software/Things_Uno_Board?ref_type=heads#getting-started)
- For Arduino Microcontrollers [here](https://gitlab.com/abdularshadvs/solar-battery-pack-monitoring-system-v1.0/-/tree/main/Software/ULP_LoRa_Board?ref_type=heads#getting-started)


### License
-----------

This project is licensed under the MIT License - see the LICENSE.md file for details



